package com.sabre.sample;

public class GreatCircleDistance {

    private GreatCircleDistance() {
    }

    private static final double DEGREES_TO_RADIANS_CONVERSION_FACTOR = 57.2958;
    private static final double RADIUS_OF_EARTH = 6378388.0;
    private static final double NUMBER_OF_METERS_IN_KILOMETER = 1000.0;

    public static double getDistanceInKilometers(double pFromLatitude, double pFromLongitude,
                                                 double pToLatitude, double pToLongitude)
    {
        if ((Math.abs(pFromLatitude - pToLatitude) + Math.abs(pFromLongitude - pToLongitude)) < 0.01)
        {
            return 0.0;
        }
        double fromLatitudeInRadians = convertDegreesToRadians(pFromLatitude);
        double fromLongitudeInRadians = convertDegreesToRadians(pFromLongitude);
        double toLatitudeInRadians = convertDegreesToRadians(pToLatitude);
        double toLongitudeInRadians = convertDegreesToRadians(pToLongitude);
        double fSinPhi = Math.sin(fromLatitudeInRadians);
        double tSinPhi = Math.sin(toLatitudeInRadians);
        double fCosPhi = Math.cos(fromLatitudeInRadians);
        double tCosPhi = Math.cos(toLatitudeInRadians);
        double cosDiff = Math.cos(toLongitudeInRadians - fromLongitudeInRadians);
        double cosSigma = (fSinPhi * tSinPhi) + (fCosPhi * tCosPhi * cosDiff);
        double sigma = Math.acos(cosSigma);
        double distance = RADIUS_OF_EARTH * sigma;

        double sinPhiAdd = fSinPhi + tSinPhi;
        double sinPhiSub = fSinPhi - tSinPhi;
        double capA = sinPhiAdd * sinPhiAdd;
        double capB = sinPhiSub * sinPhiSub;
        double sinSigma = 3.0 * Math.sin(sigma);
        double cosSigma1 = Math.cos(sigma);

        double p = 5369.0 * (sinSigma - sigma) / (1.0 + cosSigma1);
        double q = 5369.0 * (sinSigma + sigma) / (1.0 - cosSigma1);
        return (distance + (p * capA) - (q * capB)) / NUMBER_OF_METERS_IN_KILOMETER;
    }

    private static double convertDegreesToRadians(double pDegrees)
    {
        return pDegrees / DEGREES_TO_RADIANS_CONVERSION_FACTOR;
    }

}
