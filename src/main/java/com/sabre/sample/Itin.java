package com.sabre.sample;

import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.Itinerary;

import java.util.List;

public class Itin implements Itinerary
{
    private final String deptArp;
    private final String arrArp;
    private final int distance;
    private final int deptTime;
    private final int arrTime;
    private final int elapsedTime;
    private final Frequency frequency;
    private final List<FlightLeg> legs;

    public Itin(String deptArp, String arrArp, int deptTime, int arrTime, int elapsedTime, int distance, Frequency frequency, List<FlightLeg> legs)
    {
        this.deptArp = deptArp;
        this.arrArp = arrArp;
        this.distance = distance;
        this.deptTime = deptTime;
        this.arrTime = arrTime;
        this.elapsedTime = elapsedTime;
        this.legs = legs;
        this.frequency = frequency;
    }

    @Override
    public Frequency getFrequency()
    {
        return frequency;
    }

    @Override
    public String getDeptArp()
    {
        return deptArp;
    }

    @Override
    public String getArvlArp()
    {
        return arrArp;
    }

    @Override
    public int getDistance()
    {
        return distance;
    }

    @Override
    public int getDeptTime()
    {
        return deptTime;
    }

    @Override
    public int getArvlTime()
    {
        return arrTime;
    }

    @Override
    public int getElapsedTime()
    {
        return elapsedTime;
    }

    @Override
    public List<FlightLeg> getLegs()
    {
        return legs;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Itin)) return false;

        Itin itin = (Itin) o;

        if (arrTime != itin.arrTime) return false;
        if (deptTime != itin.deptTime) return false;
        if (distance != itin.distance) return false;
        if (elapsedTime != itin.elapsedTime) return false;
        if (!arrArp.equals(itin.arrArp)) return false;
        if (!deptArp.equals(itin.deptArp)) return false;
        if (!frequency.equals(itin.frequency)) return false;
        if (!legs.equals(itin.legs)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = deptArp.hashCode();
        result = 31 * result + arrArp.hashCode();
        result = 31 * result + distance;
        result = 31 * result + deptTime;
        result = 31 * result + arrTime;
        result = 31 * result + elapsedTime;
        result = 31 * result + frequency.hashCode();
        result = 31 * result + legs.hashCode();
        return result;
    }
}
