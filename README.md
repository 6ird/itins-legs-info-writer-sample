# Itinerary and Legs Reader Writer #

This is a sample project to demonstrate writing itinerary and leg information from connection editing extension point.

## Usage ##
Download project and create package(jar).
Deploy on domain under <DomainHome>/airflite/extension-points/customer-implementaions 

During analysis run(say with ID 1234), it will start creating info file <worksets>/WORKSET1234/ItinLegInfo.dat

The output file can later be used to read and create itinerary and legs for testing purpose using sample reader, ItinsAndLegsReader.readItins(Reader reader) 
reads above file and returns map of Itins by market i.e Map<String, List<Itinerary>>

## Writer ##

Sample writer ItinsLegsInfoWriter writes the Itinerary information into a file <WorksetDir>/ItinLegInfo.dat

File Format:

Dept Airport, Arrival Airport, Frequency, Dept Time, Arr Time, Elapsed Time, Distance, No Of Legs, [LEGINFO: Origin, Destination, Airline, Flight Number, Flight Number Suffix, Frequency, Dept Time, Arr Time, Elapsed Time, Distance, Is Operational, Op Airline, Op Flight Number]...
Market:MAALHR:Size:1

MAA,LHR,SUN#MON#TUE#WED#FRI#SAT,331,705,644,5120,1,[MAA,LHR,BA,36, ,SUN#MON#TUE#WED#FRI#SAT,331,705,644,5120,true,BA,36]

Market:HYDLHR:Size:2

HYD,LHR,MON#TUE#WED#THU,435,780,615,4813,1,[HYD,LHR,BA,276, ,MON#TUE#WED#THU,435,780,615,4813,true,BA,276]

HYD,LHR,SUN#FRI#SAT,435,775,610,4813,1,[HYD,LHR,BA,276, ,SUN#FRI#SAT,435,775,610,4813,true,BA,276]
